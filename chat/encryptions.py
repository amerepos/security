import binascii
import random
import math


def private_encrypt(key, n, text):
    print('ENCRYPTING')
    print(type(text))
    print((text))
    print(type(key))
    print(type(n))
    print(key)
    print(n)
    print()
    text = text.encode()
    text = binascii.b2a_hex(text)
    text = str(int.from_bytes(text, byteorder='big'))
    text = [int(text[i]) for i in range(0,len(text))]
    text = [str((p ** key) % n) for p in text]
    return '-'.join(text)


def private_decrypt(key, n, cipher):
    print('DECRYPTING')
    print(type(cipher))
    print(type(key))
    print(type(n))
    print(key)
    print(n)
    print()
    cipher = [int(c) for c in cipher.split('-')]
    cipher = [str((p ** key) % n) for p in cipher]
    cipher = int(''.join(cipher))
    cipher = cipher.to_bytes(math.ceil(math.log(cipher,2)/8),byteorder='big')
    cipher = cipher.decode()
    plain_text = bytearray.fromhex(str(cipher)).decode()
    print("COMPLETE")
    print(plain_text)
    return plain_text


def public_encrypt(key, n, text):
    print('ENCRYPTING')
    print(type(text))
    print((text))
    print(type(key))
    print(type(n))
    print(key)
    print(n)
    print()
    text = text.encode()
    text = binascii.b2a_hex(text)
    text = str(int.from_bytes(text, byteorder='big'))
    text = [int(text[i]) for i in range(0,len(text))]
    text = [str((p ** key) % n) for p in text]
    return '-'.join(text)


def public_decrypt(key, n, cipher):
    print('DECRYPTING')
    print(type(cipher))
    print(type(key))
    print(type(n))
    print(key)
    print(n)
    print()
    cipher = [int(c) for c in cipher.split('-')]
    cipher = [str((p ** key) % n) for p in cipher]
    cipher = int(''.join(cipher))
    cipher = cipher.to_bytes(math.ceil(math.log(cipher,2)/8),byteorder='big')
    cipher = cipher.decode()
    plain_text = bytearray.fromhex(str(cipher)).decode()
    print("COMPLETE")
    print(plain_text)
    return plain_text


def get_crypto_keys():
    primes = open('chat/primes').read().split()[100:150]
    p, q = int(random.choice(primes)), int(random.choice(primes))

    # Compute n = pq.
    n = p * q

    # Compute λ(n) = lcm(φ(p), φ(q)) = lcm(p − 1, q − 1),
    # where λ is Carmichael's totient function. This value is kept private.
    a = p - 1
    b = q - 1
    totient = int(a * b / math.gcd(a, b))

    # Choose an integer e such that 1 < e < λ(n) and gcd(e, λ(n)) = 1; i.e., e and λ(n) are coprime.
    while 1:
        e = random.randint(1, totient)
        if math.gcd(e, totient) is 1:
            break

    k = 0
    while 1:
        k += 1  # random.randint(1,999)
        print(k)
        d = (1 + k * totient) / e
        if float(d).is_integer():
            break
    d = int(d)

    return d, n, e


def MD5(password):
    # convert to bytes
    password = password.encode()
    password = binascii.b2a_hex(password)

    original_length = len(password)
    pad_size = (64 - original_length) * 8 - 1

    password = int.from_bytes(password, byteorder='little')  # int representation

    password = password << 1  # padding
    password += 1  # padding
    password = password << pad_size  # padding

    password += original_length * 8

    password = password.to_bytes(64, byteorder='little')  # hex representation

    split = []
    j = 0
    for i in range(4, 64, 4):
        split.append(int.from_bytes(password[j:i], byteorder='little'))
        j = i

    password = split

    k = []
    for i in range(0, 64):
        k.append(math.floor(2 ** 32 * abs(math.sin(i + 1))))

    s = []
    for i in range(0, 64):
        s.append(math.floor(32 * abs(math.cos(i + 1))))

    A = 102
    B = 221
    C = 246
    D = 75

    for i in range(0, 64):
        f = 0
        g = 0
        if i < 16:
            f = _F(B, C, D)
            g = i % 15
        elif i < 31:
            f = _G(B, C, D)
            g = (5 * i + 5) % 15
        elif i < 48:
            f = _H(B, C, D)
            g = (4 * i + 5) % 15
        else:
            f = _I(B, C, D)
            g = (7 * i) % 15

        f = (f + A + k[i] + password[g]) % 4294967295
        A = D
        D = C
        C = B
        B = (B + _S(f, s[i])) % 4294967295

    A = _pad(A, 32)
    B = _pad(B, 32)
    C = _pad(C, 32)
    D = _pad(D, 32)

    res = hex(A) + hex(B)[2:] + hex(C)[:2] + hex(D)[2:]

    return res


def _F(B, C, D):
    f1 = B & C
    f2 = ~B & D
    return f1 | f2


def _G(B, C, D):
    g1 = B & D
    g2 = C & ~D
    return g1 | g2


def _H(B, C, D):
    return B ^ C ^ D


def _S(val, s):
    val = val % 4294967295
    val = (((val << s) | (val >> (32 - s))) + 2 ** 32) % 4294967295
    return val


def _I(B, C, D):
    i1 = B | ~D
    return C ^ i1


def _pad(val, size):
    size -= 1
    while val < 2 ** size:
        val = val << 2
    return val