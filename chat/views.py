from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework import generics
from .models import *
from rest_framework.authtoken.models import Token
from .serializers import *
from django.http import HttpResponse, JsonResponse
from .forms import *
from .services import *
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from django.contrib.auth import authenticate
from django.shortcuts import redirect
from django.forms import formset_factory

# Create your views here.


def login(request):
    if request.method == 'GET':
        form = UserForm()
        return render(request, 'chat/login.html',{'form': form})


@api_view(["POST",'GET'])
@permission_classes((AllowAny,))
def contacts(request):
    if request.method == 'GET':
        curr_address = '/'.join(request.build_absolute_uri().split('/')[:-2]) + '/'
        user_name = Token.objects.get(key=request.session['token']).user
        user = MyUser.objects.get(name=user_name, address=curr_address)
        user = authenticate(username=user.name, password=user.password)
        if not user:
            return HttpResponse(status=403)

        contact_list = Contact.objects.filter(user=user).values('name', 'address').distinct()
        return render(request, 'chat/contacts.html', {'contacts': contact_list})

    if request.method == 'POST':
        print(request.POST)
        curr_address = '/'.join(request.build_absolute_uri().split('/')[:-2]) + '/'

        user = verify_user(curr_address,
                           request.POST['username'],
                           request.POST['password'])

        user = authenticate(username=user.name, password=user.password)
        if not user:
            return Response({'error': 'Invalid Credentials'},
                            status=404)

        token, _ = Token.objects.get_or_create(user=user)

        print(contacts)
        request.session['token'] = token.key
        contact_list = Contact.objects.filter(user=user).values('name', 'address').distinct()
        return render(request, 'chat/contacts.html', {'contacts': contact_list})

    return HttpResponse(status=500)


    # return redirect(request, '/contacts')
    # return Response({'token': token.key},
    #                 status=200)


def chat(request):
    curr_address = '/'.join(request.build_absolute_uri().split('/')[:-2]) + '/'
    user_name = Token.objects.get(key=request.session['token']).user
    my_user = MyUser.objects.get(name=user_name, address=curr_address)
    user = authenticate(username=my_user.name, password=my_user.password)
    if not user:
        return HttpResponse(status=403)


    if request.method == 'GET':
        contact_names = dict(request.GET.lists())['selected']
        contacts = Contact.objects.filter(name__in=contact_names, user=user).distinct()
        print(contacts)

        contact_users = [MyUser.objects.get(name = c.name, address=c.address) for c in contacts]
        my_contact = Contact.objects.filter(name=my_user.name, address=my_user.address).distinct()

        sent = Message.objects.filter(sender=my_user, receivers__in=list(contacts), is_e=False).distinct()

        received = Message.objects.filter(sender__in=contact_users, receivers__name__contains=my_user.name, is_e=True).distinct()


        old_messages = (sent | received).distinct()


        print('my_contact')
        print(my_contact)
        print('contacts')
        print(contacts)
        print('contact_users')
        print(contact_users)

        print('SENT')
        print(sent)
        print('RECIEVED')
        print(received)
        print('ALL TOGETHER')
        print(old_messages)



        request.session['contacts'] = list(contacts.values('name','address'))
        return render(request, 'chat/chat.html', {'contacts':contacts.values('name', 'address'), 'messages':old_messages})

    if request.method == 'POST':
        message_txt = request.POST['message']
        print(message_txt)
        contacts = request.session['contacts']
        print(contacts)
        contact_names = [a['name'] for a in contacts]
        contact_addresses = [a['address'] for a in contacts]


        receivers = Contact.objects.filter(name__in=contact_names, address__in=contact_addresses, user=user).distinct()

        contact_users = list(MyUser.objects.filter(name__in=contact_names, address__in=contact_addresses).distinct())
        my_contact = Contact.objects.filter(name=my_user.name, address=my_user.address).distinct()

        contacts = Contact.objects.filter(name__in=contact_names, address__in=contact_addresses).distinct()
        sent = Message.objects.filter(sender=my_user, receivers__in=list(contacts), is_e=False).distinct()

        received = Message.objects.filter(sender__in=contact_users, receivers__in=my_contact, is_e=True).distinct()

        old_messages = (sent | received).distinct()


        print('receivers')
        print(receivers)
        print('my_contact')
        print(my_contact)
        print('contacts')
        print(contacts)

        print('SENT')
        print(sent)
        print('RECIEVED')
        print(received)
        print('ALL TOGETHER')
        print(old_messages)

        # old_messages = Message.objects.filter(sender=my_user, receivers__in=receivers)


        # data = {
        #     'sender':my_user,
        #     'receivers':receivers,
        #     'message':message_txt
        # }
        message = Message(
            sender=my_user,
            # receivers=receivers,
            message=message_txt,
            is_e=False
        )
        message.save()
        for receiver in receivers:
            print(receiver)
            receiver.message_set.add(message)

        send_message(message)
        # serializer = MessageSerializer(data=data)
        # print(serializer)
        # if serializer.is_valid():
        #     serializer.save()



        return render(request, 'chat/chat.html',
                      {'contacts': receivers.values('name', 'address'), 'messages': old_messages})

    return HttpResponse(status=500)


def pair(request):
    curr_address = '/'.join(request.build_absolute_uri().split('/')[:-2]) + '/'
    user_name = Token.objects.get(key=request.session['token']).user
    user = MyUser.objects.get(name=user_name, address=curr_address)
    user = authenticate(username=user.name, password=user.password)
    if not user:
        return HttpResponse(status=403)

    form = PairingForm()
    if request.method == 'GET':
        return render(request, 'chat/pair.html',{'form': form, 'message':'Who would you like to pair'})

    if request.method == 'POST':
        myUser = MyUser.objects.get(user=user)
        response = hand_shake(myUser, request.POST['address'], request.POST['name'])

        if response:
            return render(request, 'chat/pair.html',{'form': form, 'message':'Pair Successful'})

        return render(request, 'chat/pair.html',{'form': form, 'message':'Pair Failed'})

    return HttpResponse(status=500)

@csrf_exempt
def handshake(request):
    if request.method == 'GET':
        data = {}

        for a in str(request.body)[2:-1].split('&'):
            a = a.split('=')
            data[a[0]] = a[1].replace('%3A',':').replace('%2F','/')

        data['public_key'] = int(data['public_key'])
        data['mod_val'] = int(data['mod_val'])

        user_name = data['user']

        curr_address = '/'.join(request.build_absolute_uri().split('/')[:-2]) + '/'

        current_user = MyUser.objects.get(address=curr_address, name=user_name)

        data['user'] = current_user.user
        contact = Contact(
            user=current_user.user,
            name=data['name'],
            address=data['address'],
            public_key=data['public_key'],
            mod_val=data['mod_val']
        )
        try:
            contact.save()
        except Exception as e:
            print(e)
            return HttpResponse(status=500)
        # serializer = ContactSerializer(data=data)
        # print(request.body)
        # print(serializer.initial_data)
        # if serializer.is_valid():
        #     print('pass')
        #     serializer.save()


        response_data = {
            # 'name': current_user.user_name,
            'public_key': current_user.public_key,
            'mod_val': current_user.mod_val
        }

        return JsonResponse(response_data, status=200)

    return HttpResponse(status=500)


@csrf_exempt
def receive(request):
    if request.method == 'POST':
        print('YOLO')
        print(request)
        print(request.body)
        data = {}
        for a in str(request.body)[2:-1].split('&'):
            a = a.split('=')
            data[a[0]] = a[1]

        data['sender_address'] = data['sender_address'].replace('%3A',':').replace('%2F','/')
        rec_names = data['receivers_name'].split('~')
        rec_adds = data['receivers_address'].split('~')
        rec_adds = [a.replace('%3A',':').replace('%2F','/') for a in rec_adds]
        rec_names = [a.replace('%3A',':').replace('%2F','/') for a in rec_names]

        ca = data['ca'].replace('%3A',':').replace('%2F','/')
        cn = data['cn']

        rcvrs = list(zip(rec_names, rec_adds))

        tag = data['message'].split('+~+')[0]
        message = data['message'].split('+~+')[1]

        print(data)
        print("TAG")
        print(tag)
        print("MESSAGE")
        print(message)


        my_user = MyUser.objects.get(name=cn, address=ca)
        message = private_decrypt(my_user.private_key, my_user.mod_val, message)

        print(my_user)
        print(data['sender_name'])
        sender = Contact.objects.get(address=data['sender_address'], name=data['sender_name'], user=my_user.user)
        tag = public_decrypt(sender.public_key, sender.mod_val, tag)

        print("TAG")
        print(tag)
        print("MESSAGE")
        print(message)


        message = Message(
            sender=MyUser.objects.get(address=data['sender_address'], name=data['sender_name']),
            message='('+tag+')  ' + message,
            is_e=True,
            # receivers=Contact.objects.filter(address__in=rec_adds, name__in=rec_names)
        )

        message.save()

        print('RCIEFISFJSDJFEW')
        print(rec_names)
        print(rec_adds)
        receivers=Contact.objects.filter(address__in=rec_adds, name__in=rec_names).distinct()
        print(receivers)
        for receiver in receivers:
            print(receiver)
            receiver.message_set.add(message)
        # message.save()
        return HttpResponse(status=200)

        # for r in rcvrs:
        #     r = MyUser.objects.filter(name=r[0], address=r[1])
        #     if r.exists():
        #         t = public_decrypt()
        #
        #
        #
        # # data['receivers']
        # print(data)
        #
        #
        #
        # data['receivers'] = [data['receivers']]
        # data['is_e'] = True
        #
        #
        # data['rec'] = data['rec'].replace('%3A',':').replace('%2F','/')
        # rec_add = data['rec'].split('+~+')[0]
        # rec_name = data['rec'].split('+~+')[1]
        #
        #
        #
        # sender = Contact.objects.get(pk=int(data['sender']))
        #
        #
        #
        #
        #
        # print(data['rec'])
        # print(my_user)
        # print(sender)





        #
        #
        #
        # my_user = MyUser.objects.get()
        # for rec in data['receivers']:
        #     rec = Contact.objects.filter(id=rec)
        #     if rec.exists():
        #         tag = public_decrypt(rec[0].public_key, rec[0].mod_val, tag)
        #         break
        #
        #
        # message = private_decrypt(, , message)


        print(data)
        serializer = MessageSerializer(data=data)
        print(serializer)
        if serializer.is_valid():
            serializer.save()
            return HttpResponse(status=200)
    return HttpResponse(status=400)





# class ListMessagesView(APIView):
#
#     def get(self, request, pk):
#         print('HIIIIII')
#         print(pk)
#         snippets = Message.objects.all()
#         serializer = MessageSerializer(snippets, many=True)
#         return Response(serializer.data)
#
#     def post(self, request, pk):
#         print('HIIIIII')
#         print(pk)
#         serializer = MessageSerializer(data=request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


# class ListMessagesView(generics.ListAPIView):
#     print('do whatever')
#     # queryset = Message.objects.all()
#     serializer_class = MessageSerializer
#
#     def get_queryset(self):
#         print('do whatever more')
#         return Message.objects.all()