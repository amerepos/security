from django.urls import path
from .views import *


urlpatterns = [
    path('login/', login),
    path('pair/', pair),
    path('contacts/', contacts),
    path('handshake/', handshake),
    path('receive/', receive),
    path('chat/', chat),


    # path('messages/', ListMessagesView.as_view()),
    # path('messages/<int:pk>/', ListMessagesView.as_view()),
]