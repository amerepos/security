from django.core.exceptions import ValidationError
import binascii
import math

class MD5(object):

    def __init__(self, min_length=8):
        self.min_length = min_length

    def validate(self, password, user):
        # convert to bytes
        password = password.encode()
        password = binascii.b2a_hex(password)

        original_length = len(password)
        pad_size = (64 - original_length) * 8 - 1

        password = int.from_bytes(password, byteorder='little')  # int representation

        password = password << 1  # padding
        password += 1  # padding
        password = password << pad_size  # padding

        password += original_length * 8

        password = password.to_bytes(64, byteorder='little')  # hex representation

        split = []
        j = 0
        for i in range(4, 64, 4):
            split.append(int.from_bytes(password[j:i], byteorder='little'))
            j = i

        password = split

        k = []
        for i in range(0, 64):
            k.append(math.floor(2 ** 32 * abs(math.sin(i + 1))))

        s = []
        for i in range(0, 64):
            s.append(math.floor(32 * abs(math.cos(i + 1))))

        A = 102
        B = 221
        C = 246
        D = 75

        for i in range(0, 64):
            f = 0
            g = 0
            if i < 16:
                f = _F(B, C, D)
                g = i % 15
            elif i < 31:
                f = _G(B, C, D)
                g = (5 * i + 5) % 15
            elif i < 48:
                f = _H(B, C, D)
                g = (4 * i + 5) % 15
            else:
                f = _I(B, C, D)
                g = (7 * i) % 15

            f = (f + A + k[i] + password[g]) % 4294967295
            A = D
            D = C
            C = B
            B = (B + _S(f, s[i])) % 4294967295

        A = _pad(A, 32)
        B = _pad(B, 32)
        C = _pad(C, 32)
        D = _pad(D, 32)

        res = hex(A) + hex(B)[2:] + hex(C)[:2] + hex(D)[2:]

        return res


def _F(B, C, D):
    f1 = B & C
    f2 = ~B & D
    return f1 | f2


def _G(B, C, D):
    g1 = B & D
    g2 = C & ~D
    return g1 | g2


def _H(B, C, D):
    return B ^ C ^ D


def _S(val, s):
    val = val % 4294967295
    val = (((val << s) | (val >> (32 - s))) + 2 ** 32) % 4294967295
    return val


def _I(B, C, D):
    i1 = B | ~D
    return C ^ i1


def _pad(val, size):
    size -= 1
    while val < 2 ** size:
        val = val << 2
    return val