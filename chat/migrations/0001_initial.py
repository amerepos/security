# Generated by Django 2.2 on 2019-05-02 19:57

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=256)),
                ('address', models.CharField(max_length=256)),
                ('public_key', models.BigIntegerField()),
                ('mod_val', models.BigIntegerField()),
                ('user', models.ForeignKey(on_delete='CASCADE', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'unique_together': {('name', 'user')},
            },
        ),
        migrations.CreateModel(
            name='MyUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('address', models.CharField(max_length=256)),
                ('name', models.CharField(max_length=256)),
                ('password', models.CharField(max_length=256)),
                ('private_key', models.BigIntegerField()),
                ('mod_val', models.BigIntegerField()),
                ('public_key', models.BigIntegerField()),
                ('user', models.ForeignKey(on_delete='CASCADE', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'unique_together': {('name', 'address')},
            },
        ),
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('message', models.TextField()),
                ('is_e', models.BooleanField()),
                ('receivers', models.ManyToManyField(to='chat.Contact')),
                ('sender', models.ForeignKey(on_delete='CASCADE', to='chat.MyUser')),
            ],
        ),
    ]
