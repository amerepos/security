from django.db import models
from django.contrib.auth.models import User
from .encryptions import *
# Create your models here.


class MyUser(models.Model):
    class Meta:
        unique_together = (("name", "address"),)

    user = models.ForeignKey(User, on_delete='CASCADE')
    address = models.CharField(max_length=256, null=False, blank=False)
    name = models.CharField(max_length=256, null=False, blank=False)
    password = models.CharField(max_length=256, null=False, blank=False)
    private_key = models.BigIntegerField(null=False, blank=False)
    mod_val = models.BigIntegerField(null=False, blank=False)
    public_key = models.BigIntegerField(null=False, blank=False)

    def __str__(self):
        return self.address + ' ~ ' + self.name


class Contact(models.Model):
    class Meta:
        unique_together = (("name", "user"),)
    user = models.ForeignKey(User, on_delete='CASCADE')
    name = models.CharField(max_length=256, null=False, blank=False)
    address = models.CharField(max_length=256, null=False, blank=False)
    public_key = models.BigIntegerField(null=False, blank=False)
    mod_val = models.BigIntegerField(null=False, blank=False)

    def __str__(self):
        return self.address + ' ~ ' + self.name


class Message(models.Model):
    sender = models.ForeignKey(MyUser, on_delete='CASCADE')
    receivers = models.ManyToManyField(Contact)
    message = models.TextField(null=False, blank=False)
    is_e = models.BooleanField()

    # def save(self, *args, **kwargs):
    #
    #     if self.is_e:
    #         tag = self.message.split(' ~ ')[0]
    #         body = self.message.split(' ~ ')[1]
    #
    #         body =
    #
    #
    #     super(Message, self).save(*args, **kwargs)

    def __str__(self):
        return self.message



